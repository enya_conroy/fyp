document.addEventListener("DOMContentLoaded", function() {
    const scrollamaInstance = scrollama();
    let intervals = []; // store intervals to clear later

    function handleStepEnter(response) {
        console.log("Entered step:", response.index);  // log when a step is entered
        const element = response.element;

        // add animation class to each line of text
        const lines = element.querySelectorAll(".line");
        lines.forEach((line, index) => {
            setTimeout(() => {
                line.classList.add("animate");
            }, index * 200); // delay each line's animation
        });

        // hide all images in the scene
        const images = element.querySelectorAll(".images img");
        images.forEach(img => {
            img.style.opacity = 0;
        });

        // clear any previous intervals
        intervals.forEach(interval => clearInterval(interval));
        intervals = [];

        // show the first image
        if (images.length > 0) {
            images[0].style.opacity = 1;
        }

        // start transitions if there are multiple images
        if (images.length > 1) {
            let currentImageIndex = 0;
            const interval = setInterval(() => {
                images[currentImageIndex].style.opacity = 0;
                currentImageIndex = (currentImageIndex + 1) % images.length;
                images[currentImageIndex].style.opacity = 1;
            }, 1500); // change image every 1.5 seconds

            // store the interval to clear it later
            intervals.push(interval);
        }
    }

    function handleStepExit(response) {
        const element = response.element;

        // remove animation class to reset the scene
        const lines = element.querySelectorAll(".line");
        lines.forEach(line => {
            line.classList.remove("animate");
        });

        // reset images opacity
        const images = element.querySelectorAll(".images img");
        images.forEach(img => {
            img.style.opacity = 0;
        });

        // clear any intervals related to image transition
        intervals.forEach(interval => clearInterval(interval));
        intervals = [];
    }
    
    // initialize Scrollama
    scrollamaInstance
        .setup({
            step: ".scene",
            offset: 0.75,
            debug: false  // enable / disable debug mode
        })
        .onStepEnter(handleStepEnter)
        .onStepExit(handleStepExit);  // Add handleStepExit to handle exit events

    // handle window resize events
    window.addEventListener("resize", scrollamaInstance.resize);

    // mute button functionality
    const muteButton = document.getElementById('mute-button');
    const backgroundMusic = document.getElementById('background-music');

    // change button icon / audio level when clicked
    muteButton.addEventListener('click', () => {
        if (backgroundMusic.muted) {
            backgroundMusic.muted = false;
            muteButton.innerHTML = '<i class="fas fa-volume-high"></i>';
        } else {
            backgroundMusic.muted = true;
            muteButton.innerHTML = '<i class="fas fa-volume-xmark"></i>';
        }
        // audio starts playing after user interaction
        backgroundMusic.play();
    });

});
